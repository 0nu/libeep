from . import pyeep


class CntBase:
    def __init__(self, handle):
        self._handle = handle
        if self._handle == -1:
            raise Exception('not a valid libeep handle')

    def __del__(self):
        if self._handle != -1:
            pyeep.close(self._handle)


class CntIn(CntBase):
    def __init__(self, handle):
        CntBase.__init__(self, handle)

    def get_channel_count(self):
        return pyeep.get_channel_count(self._handle)

    def get_channel(self, index):
        return (pyeep.get_channel_label(self._handle, index), pyeep.get_channel_unit(self._handle, index),
                pyeep.get_channel_reference(self._handle, index))

    def get_sample_frequency(self):
        return pyeep.get_sample_frequency(self._handle)

    def get_sample_count(self):
        return pyeep.get_sample_count(self._handle)

    def get_samples(self, fro, to):
        """
        Return samples [fro, to) from file.

        Returns list with len = n_chan*(to-fro)


        ret = [chan1-sample1, chan2-sample1, ..., chanN-sample1,
               ...
               chan1-sampleM, chan2-sampleM, ..., chanN-sampleM]

        """
        fro = int(fro)
        to = int(to)
        assert fro >= 0, "Negative start index."
        assert to >= fro, "Start index is not smaller than end index."
        assert to < pyeep.get_sample_count(self._handle), \
            "Only {} samples found in file.".format(pyeep.get_sample_count(self._handle))
        return pyeep.get_samples(self._handle, fro, to)

    def get_trigger_count(self):
        return pyeep.get_trigger_count(self._handle)

    def get_trigger(self, index):
        """
        Returns the trigger at index.

        trig = get_trigger(0)
        type(trig) = 6-tuple

        tuple[0] = Trigger value
        tuple[0] = 0 -> neuronavigation info
        tuple[0] = 4 -> MEP
        tuple[0] = 6 -> intrument marker

        tuple[1] = sample nr of onset
        tuple[2] = 0

        tuple[3] = None
        tuple[3] = String with information about instrument marker

        tuple[4] = None
        tuple[5] = None

        Returns
        -------
        <6-tuple> 4, onset-sample,0,None,None,None
        """
        assert index < pyeep.get_trigger_count(self._handle), \
            "Only {} triggers found in file.".format(pyeep.get_trigger_count(self._handle))
        assert index >= 0
        return pyeep.get_trigger(self._handle, index)


class CntOut(CntBase):
    def __init__(self, handle, channel_count):
        CntBase.__init__(self, handle)
        self._channel_count = channel_count

    def add_samples(self, samples):
        return pyeep.add_samples(self._handle, samples, self._channel_count)


def read_cnt(filename):
    if not filename.endswith('.cnt'):
        raise Exception('unsupported extension')
    return CntIn(pyeep.read(filename))


def write_cnt(filename, rate, channels, rf64=0):
    """
    create an object for writing a .cnt file.

    rate -- sampling rate in Herz
    channels -- list of tuples, where tuples contains three strings:
                channel label, channel reference and unit, i,e,:
                ['Cz', 'ref', 'uV')]
    rf64 -- if 0, create default 32-bit cnt data. otherwise 64 bit(for larger tan 2GB files)
    """
    if not filename.endswith('.cnt'):
        raise Exception('unsupported extension')

    channels_handle = pyeep.create_channel_info()
    for c in channels:
        pyeep.add_channel(channels_handle, c[0], c[1], c[2])

    rv = CntOut(pyeep.write_cnt(filename, rate, channels_handle, rf64), len(channels))

    pyeep.close_channel_info(channels_handle)

    return rv
